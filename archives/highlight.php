<?php
include_once 'geshi/geshi.php';

$geshi = new GeSHi('', '');

$geshi->set_header_type(GESHI_HEADER_PRE);

// Enable CSS classes. You can use get_stylesheet() to output a stylesheet for your code. Using
// CSS classes results in much less output source.
$geshi->enable_classes();

// Enable line numbers. We want fancy line numbers, and we want every 5th line number to be fancy
$geshi->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS, 5);

// Set the style for the PRE around the code. The line numbers are contained within this box (not
// XHTML compliant btw, but if you are liberally minded about these things then you'll appreciate
// the reduced source output).
$geshi->set_overall_style('font: normal normal 90% monospace; color: #000066; border: 1px solid #d0d0d0; background-color: #f0f0f0;', false);

// Set the style for line numbers. In order to get style for line numbers working, the <li> element
// is being styled. This means that the code on the line will also be styled, and most of the time
// you don't want this. So the set_code_style reverts styles for the line (by using a <div> on the line).
// So the source output looks like this:
//
// <pre style="[set_overall_style styles]"><ol>
// <li style="[set_line_style styles]"><div style="[set_code_style styles]>...</div></li>
// ...
// </ol></pre>
$geshi->set_line_style('color: #003030;', 'font-weight: bold; color: #006060;', true);
$geshi->set_code_style('color: #000020;', true);

// Styles for hyperlinks in the code. GESHI_LINK for default styles, GESHI_HOVER for hover style etc...
// note that classes must be enabled for this to work.
$geshi->set_link_styles(GESHI_LINK, 'color: #000060;');
$geshi->set_link_styles(GESHI_HOVER, 'background-color: #f0f000;');

// Use the header/footer functionality. This puts a div with content within the PRE element, so it is
// affected by the styles set by set_overall_style. So if the PRE has a border then the header/footer will
// appear inside it.
$geshi->set_header_content('<SPEED> <TIME> KDE rox');
$geshi->set_header_content_style('font-family: sans-serif; color: #808080; font-size: 70%; font-weight: bold; background-color: #f0f0ff; border-bottom: 1px solid #d0d0d0; padding: 2px;');

// You can use <TIME> and <VERSION> as placeholders
$geshi->set_footer_content('Parsed in <TIME> seconds at <SPEED>, using GeSHi <VERSION>');
$geshi->set_footer_content_style('font-family: sans-serif; color: #808080; font-size: 70%; font-weight: bold; background-color: #f0f0ff; border-top: 1px solid #d0d0d0; padding: 2px;');


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>KDE.org</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

		<!-- CSS -->
		<link href="css/reset.css" type="text/css" rel="stylesheet" media="screen" />
		<link href="css/style.typography.css" type="text/css" rel="stylesheet" media="screen" />
		<link href="css/style.css" type="text/css" rel="stylesheet" media="screen" />
		<link href="jquery_plugins/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" media="screen" />
	</head>
	<body>
		<!-- JS -->
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/smart.min.js"></script>
		<script type="text/javascript" src="jquery_plugins/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="jquery_plugins/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#content-pane img:not(.nozoom)").each(function(index, img) {
					var parent = $(img).parent()[0];
					if(parent.tagName != 'A')
						return ;

					$(parent).fancybox({
						'overlayShow'  : false,
						'transitionIn' : 'elastic',
						'transitionOut': 'elastic'
					});
				});
			});
		</script>
		
		<div id="header">
			<div id="header-content">
				<ul>
					<li class="header menu img"><a href=""><img src="kde.org.img/logo.plain.png"/></a></li>
					<li class="header menu txt"><a href=""><h1>Who we are</h1><span>Find out about KDE world-wide community</span></a></li>
					<li class="header menu txt"><a href=""><h1>What we do</h1><span>Get information on what is KDE and KDE platform</span></a></li>
					<li class="header menu txt"><a href=""><h1>Contact and Support</h1><span>Get in touch with us and receive help from community</span></a></li>
				</ul>
			</div>
			<div id="header-shade"></div>
		</div>

		<div class="wrapper">
			<div id="content">
				<div id="content-pane" class="feature">
					<h1>About KDE</h1>
					<section>
					<p class="alt">KDE is an international team co-operating on development and distribution of Free, Open Source Software</p>
					<p><strong>Our community</strong> has developed a wide variety of applications for communication, work, education and entertainment. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant, open atmosphere for experimentation.</p>
					</section>
					<hr />
					<section class="two-columns">
					<div class="visual">
						<a href="#">
						<div class="screenshot"><img src="screenshots/46-w09.png" /></div>
						<div class="description"><h1>KDE SC 4.6</h1><p>KDE is delighted to announce its latest set of releases, providing major updates to the KDE Plasma workspaces, KDE Applications and KDE Platform. These releases, versioned 4.6, provide many new features in each of KDE's three product lines.</p></div>
						<div class="fade-out"></div>
						</a>
					</div>
					<div class="visual">
					<a href="#">
					<div class="screenshot"><img src="screenshots/46-w09.png" /></div>
					<div class="description"><h1>KDE SC 4.6</h1><p>KDE is delighted to announce its latest set of releases, providing major updates to the KDE Plasma workspaces, KDE Applications and KDE Platform. These releases, versioned 4.6, provide many new features in each of KDE's three product lines.</p></div>
					<div class="fade-out"></div>
					</a>
					</div>
					</section>
					<hr />
					<h3>GeSHi - Generic Syntax Highlighter</h3>
					<p>GeSHi is a syntax highlighter for HTML, written in PHP. Basically, you input the source you want to highlight and the name of the language you want to highlight it in, and GeSHi returns the syntax-highlighted result. But it doesn't stop there - GeSHi has many powerful and unique features.</p>

					<ol>
						<li>The ability to change the styles of any highlighted element on the fly</li>
						<li>Use of CSS classes to reduce the amount of output produced (GeSHi can also produce a stylesheet to be used with a language on the fly)</li>
						<li>XHTML 1.0 Strict + CSS2 compliance</li>
						<li>Auto-caps/noncaps of keywords</li>
						<li>Line numbering (both normal and fancy forms, see the <a href="http://qbnz.com/highlighter/demo.php">demo</a></li>
						<li>And many more!</li>
					</ol>
					<hr />
					<h3>C++</h3>
<?php

$cpp = <<<CPP
int main(int argc, char *argv[])
{
    lowerIOPriority();
    lowerSchedulingPriority();
    lowerPriority();

    KAboutData aboutData("nepomukindexer", 0, ki18n("NepomukIndexer"),
                         "1.0",
                         ki18n("NepomukIndexer indexes the contents of a file and saves the results in Nepomuk"),
                         KAboutData::License_LGPL_V2,
                         ki18n("(C) 2011, Vishesh Handa"));
    aboutData.addAuthor(ki18n("Vishesh Handa"), ki18n("Current maintainer"), "handa.vish@gmail.com");
    aboutData.addCredit(ki18n("Sebastian Trüg"), ki18n("Developer"), "trueg@kde.org");
    
    KCmdLineArgs::init(argc, argv, &aboutData);
    
    KCmdLineOptions options;
    options.add("uri <uri>", ki18n("The URI provided will be forced on the resource"));
    options.add("mtime <time>", ki18n("The modification time of the resource in time_t format"));
    options.add("+[url]", ki18n("The URL of the file to be indexed"));
    options.add("clear", ki18n("Remove all indexed data of the URL provided"));
    
    KCmdLineArgs::addCmdLineOptions(options);   
    const KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    // Application
    QCoreApplication app( argc, argv );
    KComponentData data( aboutData, KComponentData::RegisterAsMainComponent );
    
    const KUrl uri = args->getOption("uri");
    const uint mtime = args->getOption("mtime").toUInt();

    if( args->count() == 0 ) {
        Nepomuk::Indexer indexer;
        if( !indexer.indexStdin( uri, mtime ) ) {
            QTextStream s(stdout);
            s << indexer.lastError();
            return 1;
        }
        else {
            return 0;
        }
    }
    else if( args->isSet("clear") ) {
        Nepomuk::clearIndexedData( args->url(0) );
        kDebug() << "Removed indexed data for" << args->url(0);
        return 0;
    }
    else {
        Nepomuk::Indexer indexer;
        if( !indexer.indexFile( args->url(0), uri, mtime ) ) {
            QTextStream s(stdout);
            s << indexer.lastError();
            return 1;
        }
        else {
            kDebug() << "Indexed data for" << args->url(0);
            return 0;
        }
    }
}
CPP;

#$geshi = $cpp, 'cpp');
$geshi->set_source($cpp);
$geshi->set_language('cpp-qt', true);

?>

    <style type="text/css">
    <!--
    <?php
        echo $geshi->get_stylesheet(true);
    ?>
    -->
    </style>

<?php
	echo $geshi->parse_code();
?>
					<hr />
					<h3>PHP</h3>
<?php

$php = '<?php
try {
    throw new Exception("Some very bad exception");
} catch(Exception $e) {
    echo $e->getMessage();
}
?>';
$geshi->set_source($php);
$geshi->set_language('php', true);

?>

    <style type="text/css">
    <!--
    <?php
        echo $geshi->get_stylesheet(true);
    ?>
    -->
    </style>

<?php
	echo $geshi->parse_code();
?>
					<hr />
					<h3>Pear</h3>
<?php

$pear = '  { package Horse;
    @ISA = qw(Animal);
    sub sound { "neigh" }
    sub name {
      my $self = shift;     # instance method, so use $self
      $$self;
    }
    sub named {
      my $class = shift;    # class method, so use $class
      my $name = shift;
      bless \$name, $class;
    }
  }';
$geshi->set_source($pear);
$geshi->set_language('perl', true);

?>

    <style type="text/css">
    <!--
    <?php
        echo $geshi->get_stylesheet(true);
    ?>
    -->
    </style>

<?php
	echo $geshi->parse_code();
?>
				</div>
			</div>
		</div>

		<div id="panel">
			<div id="panel-content">
				<img src="kde.org.img/logo.footer.png" alt="" />
			</div>
		</div>

		<div id="footer">
			<div id="footer-content">
				<section class="three-columns">
				<p><strong>Our community</strong> has developed a wide variety of applications for communication, work, education and entertainment. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant, open atmosphere for experimentation.</p>
				<p><strong>Our community</strong> has developed a wide variety of applications for communication, work, education and entertainment. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant, open atmosphere for experimentation.</p>
				<p><strong>Our community</strong> has developed a wide variety of applications for communication, work, education and entertainment. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant, open atmosphere for experimentation.</p>
				</section>
			</div>
			<div id="footer-copyright">
			</div>
		</div>
	</body>
</html>
