// fix sub nav on scroll
    var $win = $(window)
      , $nav = $('.navbar-bottom')
      , navTop = $('.navbar-bottom').length && $('.navbar-bottom').offset().top
      , isFixed = 0

    processScroll()

    $win.on('scroll', processScroll)
    $('#navSidebarNeverland').scrollspy()

    function processScroll() {
      var i, scrollTop = $win.scrollTop() + $win.height() - 40;
      if (scrollTop <= navTop && !isFixed) {
        isFixed = 1
        $nav.addClass('fixed')
      } else if (scrollTop >= navTop && isFixed) {
        isFixed = 0
        $nav.removeClass('fixed')
      }
    } 
