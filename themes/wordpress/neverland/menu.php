<?php
/*
Template Name: Menu
*/
$home_logo_wrap = '<ul class="nav">%3$s</ul>';
$defaults = array(
  'container'       => '',
  'container_class' => '',
  'container_id'	=> '',
  'menu_class'      => '', 
  'menu_id'			=> '',
  'before'          => '',
  'after'           => '',
  'link_before'     => '',
  'link_after'      => '',
  'items_wrap'      => $home_logo_wrap,
  'walker'			=> new description_walker()
);
?>
