<?php get_header(); ?>

	<!-- Example row of columns -->
	<div id="pageRow" class="row">
		<div id="page">
			<div id="contentRow" class="span12">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : ?>
						<?php the_post(); ?>
						<div class="hero-unit">
							<!--<div id="myCarousel" class="carousel">-->
							<!-- Carousel items 
								<div class="carousel-inner">
									<div class="active item">
										<a href="http://jointhegame.kde.org"><img src="http://www.kde.org/images/teaser/jointhegame.gif" alt="Join the Game"/></a>
									</div>
									<div class="item">
										<a href="http://www.kde.org/announcements/4.8/"><img src="http://www.kde.org/images/teaser/480.png" alt="Releases 4.8 Improve User Experience" /></a>
									</div>
									<div class="item">
										<a href="http://flossmanuals.net/kde-guide/"><img src="http://www.kde.org/images/teaser/KDEDevelbook.png" alt="KDE Development – A Beginner's Guide" /></a>
									</div>
									<div class="item">
										<a href="http://documentfreedom.org/"><img src="http://www.kde.org/images/teaser/Document_Freedom_Day.png" alt="Document Freedom Day March 28th 2012" /></a>
									</div>
								</div>-->
								<!-- Carousel nav 
								<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
								<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>-->
							<div class="post" id="post-<?php the_ID(); ?>">
								<h1 class="posttitle"><?php the_title(); ?></h1>
							</div>
						</div>
						<div class="entry">
							<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
							<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
			</div>
		</div>
		<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
		<div class="widget-area row">
			<?php dynamic_sidebar( 'footer-1' ); ?>
		</div>
		<?php endif; ?>
	</div>
	<?php get_footer(); ?>
