<?php
/**
 * @ver 0.2
 * @license BSD License - www.opensource.org/licenses/bsd-license.php
 *
 * Copyright (c) 2012 Ingo Malchow <imalchow@kde.org>
 * All rights reserved. Do not remove this copyright notice.
 *
 * @package WordPress
 * @subpackage Neverland
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>  
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php bloginfo('name'); ?><?php wp_title(' - '); ?></title>
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->
	<!-- CSS -->
	<link href="<?php bloginfo( 'template_url' ); ?>/bootstrap.css" rel="stylesheet" media="screen" />
	<link href="<?php bloginfo( 'template_url' ); ?>/bootstrap-responsive.css" rel="stylesheet" media="screen" />
	<link href="<?php bloginfo( 'template_url' ); ?>/bootstrap-wordpress.css" rel="stylesheet" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="icon" href="http://cdn.kde.org/img/favicon.ico" />
	<link rel="shortcut icon" href="http://cdn.kde.org/img/favicon.ico" />
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
	<?php wp_head(); ?>
	<?php include (TEMPLATEPATH . "/menu.php" );?>
</head>
<body class="no-js" data-spy="scroll">

	<div class="navbar navbar-fixed-top Neverland">
		<div class="navbar-inner">
				<div class="row">
					<div class="span12">
						<a class="brand" href="<?php echo home_url('/')?>"><img src="http://cdn.kde.org/img/logo.plain.png" width="43" height="43"/></a>
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>
						<div class="nav-pills nav-collapse">
							<?php wp_nav_menu($defaults); ?>
							<ul class="nav pull-right">
								<li>
									<?php get_search_form(); ?>
								</li>
							</ul>
						</div>

					</div>
				</div>
		</div>
	</div>

	<div class="container Neverland">
	<?php wp_meta(); ?>