<?php get_header(); ?>

<!-- Example row of columns -->
<div id="pageRow" class="row">
	<div id="page" class="span12">
		<div id="contentRow">
		<?php if (have_posts()) : ?>
			<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

				<?php endwhile; // end of the loop. ?>
		<?php endif; ?>
		</div>
	</div>
	<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
