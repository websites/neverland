<?php
$subpages = array(
	'depth'        => 1,
	'show_date'    => '',
	'date_format'  => get_option('date_format'),
	'child_of'     => 0,
	'exclude'      => '',
	'include'      => '',
	'title_li'     => '',
	'echo'         => 0,
	'authors'      => '',
	'sort_column'  => 'menu_order, post_title',
	'link_before'  => '<h3>',
	'link_after'   => '</h3>',
	'walker'       => '' 
);
if ( is_page() && $post->post_parent ) {
	$subpages['child_of'] = $post->post_parent;
} else {
	$subpages['child_of'] = $post->ID;
}
$children = wp_list_pages($subpages);

if($children){
	?>
	<div class="span3">
				<ul id="navSidebarNeverland" class="nav nav-list Neverland"> 
  			<?php echo $children; ?>
  			<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<li class="Neverland widget-area">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</li>
		<?php endif; ?>
		<?php wp_nav_menu( array( 'theme_location' => 'sidebar-menu1' ) ); ?>
		</ul>
	
	</div>
<?php } ?>