<?php get_header(); ?>

<div class="row">
	<div id="content">
		<div id="content-pane" class="span12">
			<?php if (have_posts()) : ?>
				<h1 class="pagetitle">Search Results</h1>
				<ul class="pager">
					<li><?php next_posts_link('&laquo; Previous Entries') ?></li>
					<li><?php previous_posts_link('Next Entries &raquo;') ?></li>
				</ul>
				<table class="table table-striped table-bordered table-condensed">
					<?php while (have_posts()) : ?>
					<tr>
						<td>
							<?php the_post(); ?>
							<div class="post">
								<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h3>
								<small><?php the_excerpt(); ?></small>
							</div>
						</td>
					</tr>
				<?php endwhile; ?>
				</table>
				<ul class="pager">
					<li><?php next_posts_link('&laquo; Previous Entries') ?></li>
					<li><?php previous_posts_link('Next Entries &raquo;') ?></li>
				</ul>
			<?php else : ?>
				<div class="alert alert-error">
					<strong class="posttitle center">No page found.</strong>
					<p> Try a different search?</p>
					<?php get_search_form(); ?>
					<div class="clearfix"></div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>

