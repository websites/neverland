<?php get_header(); ?>

<div class="wrapper">
	<div id="content">
		<div id="content-pane" class="poster">
			<section class="banner two-columns">  
			<!-- Currently empty -->
			</section>
			<?php include (TEMPLATEPATH . '/searchform.php'); ?>
			<h2 class="posttitle">Archives by Month:</h2>
			<ul>
				<?php wp_get_archives('type=monthly'); ?>
			</ul>

			<h2 class="posttitle">Archives by Subject:</h2>
			<ul>
				<?php wp_list_categories(); ?>
			</ul>
		</div>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

