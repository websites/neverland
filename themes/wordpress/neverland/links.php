<?php get_header(); ?>

<div class="wrapper">
	<div id="content">
		<div id="content-pane" class="poster">
			<section class="banner two-columns">  
			<!-- Currently empty -->
			</section>
			<h2 class="posttitle">Links:</h2>
			<ul>
				<?php get_links_list(); ?>
			</ul>
		</div>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

