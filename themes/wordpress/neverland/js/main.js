var fullHeight;
var pgHeight;
var winHeight;
var headerHeight;
var footerHeight;
var barHeight;
var selCount;
var selHeight;

$(window).load(function() {
	var idx = 100;
		
	for (var i = 1; i <= selCount; i++)
	{
		$('#selector-' + i.toString()).css('zIndex', idx.toString());
		idx++;
	}
	
	doMagic();
});

$(window).scroll(function () { 
	doMagic();
});

$(window).resize(function() {
	doMagic();
});

function doMagic() 
{
	fullHeight = $(document).height();
	pgHeight = $(document).height() - $(this).height() - $('#footer').height() - 95;
	winHeight = $(this).height();
	headerHeight = $('#header').height();
	footerHeight = $('#footer').height();
	barHeight = pgHeight + $(this).height();
	selCount = $('#selector-pane ul').children().length;
	selHeight = $('#selector-pane').height();
	
	var top = $(this).scrollTop();
	
	// Floating footer	
	if (top > pgHeight)
	{   
		$('#panel').css({
		position: 'absolute',
		top: barHeight.toString() + 'px'
		});
	}

	if (top <= (pgHeight + 30))
	{
		$('#panel').css({
		position: 'fixed',
		top: ($(this).height() - 30).toString() + 'px'
		});
	}

	
	// Sidebar accordion
	if (top > (pgHeight - winHeight))
	{
		var i = 2;
		var classes;
		
		while (true) 
		{
			classes = $('#selector-' + (i - 1).toString()).attr('class');
			
			if (classes.indexOf('active') == -1)
			{
				$('#selector-' + i.toString()).css('marginTop', '-48px');
				selHeight = $('#selector-pane').height();
			}
			
			i++;
			
			if (i > selCount)
			{
				break;
			}
			else if (selHeight <= (pgHeight - top + selHeight))
			{
				break;
			}
		}
		
		if ($('#selector-pane').css('position') != 'absolute')
		{
			$('#selector-pane').css({
				position: 'absolute',
				top: (fullHeight - footerHeight - headerHeight - selHeight - 100).toString() + 'px'
			});
		}
	}
	
	if (top <= (pgHeight + 30))
	{
		for (var i = selCount; i >= 2; i--)
		{
			$('#selector-' + i.toString()).css('marginTop', '0px');
			selHeight = $('#selector-pane').height();
			
			if ((selHeight + 48) > (pgHeight - top + selHeight))
			{
				break;
			}
		}
		
		$('#selector-pane').css({
			position: 'fixed',
			top: '130px'
		});
	}
}
