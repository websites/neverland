/* Note: This is a very smart JS */

/* Constants */
var minFloatingHeader = 600;
var minFloatingFooter = 0;
var minFloatingSidebar = 600;

var fontSizeMobiles = 10;
var fontSizeTabs = 12;
var fontSizeStandard = 14;
var fontSizeLarge = 16;

var htmlLoader = '<div id="loader">Loading</div>';

/* Globals */
var pageHeight;
var viewHeight;
var headerHeight;
var footerHeight;
var panelHeight;
var sidebarHeight;
var sidebarOrigHeight;
var sidebarCount;
var sidebarTop;
var sidebarBottom;
var sidebarFactor;
var sidebarBlockHeights;
var pageTop;
var urls;

/* Check if jQuery is loaded */
if (!jQuery) {
    console.log('Error: jQuery not loaded');
    throw ('jQueryNotLoadedException'); /* Throw to stop execution */
} else {
    $ = jQuery;
}

/* Verify that the body tag is loaded */
if ($('body').length == 0) {
    console.log('Error: Body tag not found. Place the JS inside the body tag to fix this.');
    throw ('BodyTagNotFound'); /* Throw to stop execution */
}

/* Triggers */
$(window).bind('blur', preInitHost);
$(window).bind('load', initHost);
$(window).bind('load', postInitHost);
$(window).bind('resize', cacheHost);
$(window).bind('load scroll resize', actionHost);

/* Trigger pre-initialization actions */
$(window).trigger('blur');

/* Timer */
setInterval(actionHost, 500);

/* Pre-initialization handler */
function preInitHost() {
    try {
        if ($('#loader').length == 0) {
            $('body').append(htmlLoader);
            $('#loader').css({
                left: (($(document).width() / 2) - 50).toString() + 'px'
            });
        }
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Initialization handler */
function initHost() {
    try {
        urls = new Array();
        sidebarBlockHeights = new Array();
        sidebarBlock = $('#selector-pane > ul');

        for (var idx = 1; idx <= $('#selector-pane > ul').children().length; idx++) {
            $('#selector-' + idx.toString()).css('zIndex', idx.toString());
        }

        cacheHost();
        doShowLoader();
        doFocusSidebar();
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Post-initialization handler */
function postInitHost() {
    try {
        $('#loader').fadeOut(500);
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Cache handler */
function cacheHost() {
    try {
        getScreenData(true);
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Action handler */
function actionHost() {
    try {
        getScreenData(false);
        doAdjustFontSize();
        doFloatingHeader();
        doFloatingPanel();
        doFloatingSidebar();
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Fetch element sizes on the screen */
function getScreenData(cache) {
    try {
        pageHeight = $(document).height();
        viewHeight = $(window).height();
        headerHeight = $('#header').outerHeight();
        footerHeight = $('#footer').outerHeight();
        panelHeight = $('#panel').outerHeight();
        pageTop = $(window).scrollTop();

        if ($('#selector-pane').length > 0) {
            sidebarHeight = $('#selector-pane').outerHeight();
            sidebarCount = $('#selector-pane > ul').children().length;
            sidebarTop = $('#selector-pane').offset().top;
            sidebarFactor = parseInt($('body').css('fontSize')) * 3;

            if (cache) {
                sidebarOrigHeight = $('#selector-pane').outerHeight() + panelHeight;
            }

            for (var idx = 1; idx <= sidebarCount; idx++) {
                sidebarBlockHeights[idx] = $('#selector-' + idx.toString()).height() - 2 - $('#selector-' + idx.toString() + ' > a > h1').outerHeight() + parseInt($('#selector-' + idx.toString()).css('paddingTop'));

                if (cache) {
                    sidebarOrigHeight -= sidebarBlockHeights[idx];
                }
            }
        }
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Show a loading box when navigating through pages */
function doShowLoader() {
    try {
        $('a')
        .unbind('click')
        .bind('click', function () {
            if ($(this).attr('href').indexOf('#') != 0) {
                $('#loader').show();
                
                setTimeout(function() {
                    $('#loader').fadeOut(500);
                }, 15000);
            }
            
            return true;
        });
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Adjust font size as per available view area */
function doAdjustFontSize() {
    try {
        if (viewHeight <= 300) {
            $('body').css('fontSize', fontSizeMobiles.toString() + 'px');
        } else if (viewHeight <= 500) {
            $('body').css('fontSize', fontSizeTabs.toString() + 'px');
        } else if (viewHeight <= 700) {
            $('body').css('fontSize', fontSizeStandard.toString() + 'px');
        } else if (viewHeight <= 900) {
            $('body').css('fontSize', fontSizeLarge.toString() + 'px');
        }

        getScreenData(false);
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Show a floating header depending upon the view area */
function doFloatingHeader() {
    try {
        if (viewHeight >= minFloatingHeader) {
            if ($('#header').css('position') == 'absolute') {
                $('#header').css({
                    position: 'fixed',
                    top: '-' + headerHeight + 'px'
                });

                $('#header').stop(false, false);
                $('#header').animate({
                    top: '0px'
                });
            }

            if ($('#selector-pane').css('position') == 'fixed') {
                $('#selector-pane').css({
                    top: headerHeight.toString() + 'px'
                });
            }
        } else {
            if ($('#header').css('position') == 'fixed') {
                $('#header').stop(false, false);
                $('#header').animate({
                    top: '-' + headerHeight + 'px'
                }, function () {
                    $('#header').css({
                        position: 'absolute',
                        top: '0px'
                    });
                });
            }

            if ($('#selector-pane').css('position') == 'fixed' && sidebarTop > headerHeight) {
                $('#selector-pane').css({
                    top: (viewHeight / 10).toString() + 'px'
                });
            }
        }
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Show a floating footer panel */
function doFloatingPanel() {
    try {
        if (viewHeight >= minFloatingFooter) {
            if (pageTop > (pageHeight - viewHeight - footerHeight + panelHeight)) {
                var panelAbsoluteTop = pageHeight - footerHeight - panelHeight;

                $('#panel').css({
                    position: 'absolute',
                    top: panelAbsoluteTop.toString() + 'px'
                });
            } else {
                var panelFixedTop = viewHeight - panelHeight;

                $('#panel').css({
                    position: 'fixed',
                    top: panelFixedTop.toString() + 'px'
                });
            }
        } else {
            var panelAbsoluteTop = pageHeight - footerHeight - panelHeight;

            $('#panel').css({
                position: 'absolute',
                top: panelAbsoluteTop.toString() + 'px'
            });
        }
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Show a floating/folding sidebar */
function doFloatingSidebar() {
    try {
        var isHeaderVisible = false;
        var isFooterVisible = false;
        var viewArea;

        if (viewHeight >= minFloatingHeader) {
            isHeaderVisible = true;
        }

        if (pageTop > (pageHeight - viewHeight - footerHeight + panelHeight)) {
            isFooterVisible = true;
        }

        viewArea = viewHeight - panelHeight - (viewHeight / 10);
        viewArea -= (isHeaderVisible ? headerHeight : 0);
        viewArea -= (isFooterVisible ? (viewHeight - (pageHeight - footerHeight - pageTop)) : 0);

        if ((sidebarHeight + sidebarFactor) <= viewArea) {
            for (i = sidebarCount; i >= 2; i--) {
                var previousBlock = $('#selector-' + (i - 1).toString());
                var thisBlock = $('#selector-' + i.toString());

                if (parseInt(thisBlock.css('marginTop')) != 0) { /* Show the arrow */
                    if (previousBlock.hasClass('active-noarrow')) {
                        previousBlock.removeClass('active-noarrow');
                        previousBlock.addClass('active');
                    }

                    thisBlock.css({
                        'marginTop': '0px'
                    });
                    sidebarHeight += sidebarBlockHeights[i - 1];
                }

                if ((sidebarHeight + sidebarBlockHeights[i - 1]) > viewArea) {
                    break;
                }
            }
        }

        if (sidebarHeight > viewArea) {
            for (i = 2; i <= sidebarCount; i++) {
                var previousBlock = $('#selector-' + (i - 1).toString());
                var thisBlock = $('#selector-' + i.toString());

                if (!thisBlock.is(':animated') && parseInt(thisBlock.css('marginTop')) == 0) { /* Hide the arrow */
                    if (previousBlock.hasClass('active')) {
                        previousBlock.removeClass('active');
                        previousBlock.addClass('active-noarrow');
                    }

                    thisBlock.css({
                        'marginTop': '-' + sidebarBlockHeights[i - 1].toString() + 'px'
                    });
                    sidebarHeight -= sidebarBlockHeights[i - 1];
                }

                if (sidebarHeight <= viewArea) {
                    break;
                }
            }
        }

        /* Make it float! */
        var floatPoint = (pageHeight - footerHeight - panelHeight - sidebarOrigHeight - 60);

        if (pageTop < headerHeight) {
            $('#selector-pane').css({
                position: 'fixed',
                top: headerHeight.toString() + 'px'
            });

        } else if (pageTop < floatPoint) {
            if ($('#selector-pane').css('position') != 'fixed') {
                $('#selector-pane').css({
                    position: 'fixed',
                    top: ((isHeaderVisible ? headerHeight : 20)).toString() + 'px'
                });
            }
        } else if (sidebarTop > floatPoint) {
            if ($('#selector-pane').css('position') != 'absolute') {
                $('#selector-pane').css({
                    position: 'absolute',
                    top: floatPoint.toString() + 'px'
                });
            }
        }
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}

/* Focus on sidebar elements */
function doFocusSidebar() {
    try {
        $('#selector-pane > ul > li')
            .unbind('mouseover mouseout')
            .bind('mouseover', function () {
                $(this).css('zIndex', '1000');
            })
            .bind('mouseout', function () {
                var index = $(this).attr('id').replace('selector-', '');
                $(this).css('zIndex', index.toString());
            });
    } catch (e) {
        console.log('Error: ' + e.message);
    }
}