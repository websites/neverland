<?php
class description_walker extends Walker_Nav_Menu
{
      function start_el(&$output, $item, $depth, $args)
      {
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

           $class_names = $value = '';

           $classes = empty( $item->classes ) ? array() : (array) $item->classes;

           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="header menu txt '. esc_attr( $class_names ) . '"';

           $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

           $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
           $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
           $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

           $prepend = '<h2>';
           $append = '</h2>';
           $description  = ! empty( $item->description ) ? '<small>'.esc_attr( $item->description ).'</small>' : '';

           if($depth != 0)
           {
                     $description = $append = $prepend = "";
           }

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).'<br />';
            $item_output .= $description.$args->link_after;
            $item_output .= $append.'</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            }
}


add_action( 'after_setup_theme', 'neverland_setup' );

if ( ! function_exists( 'neverland_setup' ) ):
	function neverland_setup() {
		add_editor_style();
		add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		load_theme_textdomain( 'neverland', TEMPLATEPATH . '/languages' );
		$locale = get_locale();
		$locale_file = TEMPLATEPATH . "/languages/$locale.php";
		if ( is_readable( $locale_file ) )
			require_once( $locale_file );
		register_nav_menus( array(
			'primary' => __( 'Primary Navigation', 'neverland' ),
		) );
		if ( ! defined( 'NO_HEADER_TEXT' ) )
			define( 'NO_HEADER_TEXT', true );
}
endif;

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ));

show_admin_bar(FALSE);

add_filter("login_redirect", "subscriber_login_redirect", 10, 3);

function subscriber_login_redirect($redirect_to, $request, $user){  
	if(isset($user->roles) && is_array($user->roles)){
		if(in_array('administrator', $user->roles)) return home_url('/wp-admin/');
	}
	return home_url();
}

function get_subpages($id) {
	$my_wp_query = new WP_Query();
$all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));

// Get the page as an Object
$portfolio =  get_page_by_title('What we do');

// Filter through all pages and find Portfolio's children
$portfolio_children = get_page_children($portfolio->ID, $all_wp_pages);

// echo what we get back from WP to the browser
echo '<pre>'.print_r($portfolio_children,true).'</pre>';
}

/* Let's add some widget areas
 * Namely a sidebar area, a bottom and a footer area 
 */
function neverland_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'neverland' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Bottom Area', 'neverland' ),
		'id' => 'footer-1',
		'description' => __( 'Area above footer bar but below content', 'neverland' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s span4">',
		'after_widget' => "</aside>",
		'before_title' => '<strong class="widget-title">',
		'after_title' => '</strong>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'neverland' ),
		'id' => 'footer-2',
		'description' => __( 'Site footer', 'neverland' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s container"><div class="row">',
		'after_widget' => "</div></aside>",
		'before_title' => '<strong class="widget-title span12">',
		'after_title' => '</strong>',
	) );
}
add_action( 'widgets_init', 'neverland_widgets_init' );

function register_kde_menus() {
	register_nav_menus(
		array( 'header-menu' => __( 'Header Menu' ),
			'sidebar-menu1' => 'Who we are',
			'sidebar-menu2' => 'What we do',
			'sidebar-menu3' => 'Contact & Support'
    )
  );
}
add_action( 'init', 'register_kde_menus' );