<?php get_header(); ?>

	<!-- Example row of columns -->
	<div id="pageRow" class="row">
		<div id="page" class="span7">
			<div id="contentRow" class="row">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : ?>
					<?php the_post(); ?>
					<div class="post" id="post-<?php the_ID(); ?>">
						<h2 class="posttitle"><?php the_title(); ?></h2>
						<div class="entry">
							<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
							<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
		</div>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
