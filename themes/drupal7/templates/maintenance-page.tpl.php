<?php

/**
 * @file
 * Implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in page.tpl.php.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 * @see neverland_process_maintenance_page()
 */
?>
<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
    <head>
        <?php print $head; ?>

        <title>
            <?php print $head_title; ?>
        </title>

        <?php print $styles; ?>
        <?php print $scripts; ?>
        <link href="//cdn.kde.org/css/min/bootstrap.css" rel="stylesheet" type="text/css" media="screen, projection" />
        <link href="//cdn.kde.org/css/min/bootstrap-drupal.css" rel="stylesheet" type="text/css" media="screen, projection" />
    </head>

    <body class="<?php print $classes; ?>" <?php print $attributes;?>>
        <div class="navbar">
            <div class="navbar-neverland">
                <div class="container">
                    <a class="brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                        <?php if ($logo): ?>
                            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                        <?php endif; ?>

                        <?php if ($site_name): ?>
                            <?php print $site_name; ?>
                        <?php endif; ?>
                    </a>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="hero-unit">
                        <?php if ($title): ?>
                            <h1 id="page-title">
                                <?php print $title; ?>
                            </h1>
                        <?php endif; ?>

                        <?php print $content; ?>
                        
                        <?php if ($messages): ?>
                            <div id="messages">
                                <?php print $messages; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
