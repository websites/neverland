<div class="<?php print $classes; ?> comment-box clearfix"<?php print $attributes; ?>>
    <?php print render($title_prefix); ?>

    <h4 <?php print $title_attributes; ?>>
        <?php print $title; ?>
    </h4>

    <?php print render($title_suffix); ?>

    <div class="content"<?php print $content_attributes; ?>>
        <?php
            // We hide the comments and links now so that we can render them later.
            hide($content['links']);
            print render($content);
        ?>

        <?php if ($signature): ?>
            <div class="user-signature clearfix">
                <?php print $signature; ?>
            </div>
        <?php endif; ?>
    </div>

    <hr />

    <div class="comment-ts pull-left">
        <?php print t('By') . neverland_profile_url('name', $author) . t('at') . " {$created}"; ?>
    </div>
    
    <?php print render($content['links']); ?>
</div>
