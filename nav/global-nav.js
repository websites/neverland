/**
* KDE-www Global Navigation
*
* @category Navigation file
* @copyright (c) 2012 KDE Webteam
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/* Define the primary global nav object */
var $nav = {
    /* Core variables */
    sites: new Array(),
    browserLang: document.documentElement.lang.toLowerCase(),

    /* Localization variables */
    langs: ['en-us'],
    lang: 'en-us',

    /* Hook mechanism */
    hooks: new Array(),
    runHooks: function() {
        if (this.hooks.length > 0) {
            for (idx in this.hooks) {
                if (typeof(this.hooks[idx]) == 'function') {
                    this.hooks[idx]();
                }
            }
        }
    },
    
    /* Entry point */
    init: function() {
        /* Check if language is supported */
        if (this.browserLang.length > 0) {
            for (idx in this.langs) {
                if (this.langs[idx] == this.browserLang) {
                    this.lang = this.browserLang;
                    break;
                }
            }
        }

        /* Add the links script */
        var links       = document.createElement('script');
            links.type  = 'text/javascript';
            links.src   = '//cdn.kde.org/nav/global-nav-links.' + this.lang + '.js';

        /* Add the parser script */
        var parser      = document.createElement('script');
            parser.type = 'text/javascript';
            parser.src  = '//cdn.kde.org/nav/global-nav-parser.js';

        document.getElementsByTagName('head')[0].appendChild(links);
        document.getElementsByTagName('head')[0].appendChild(parser);
    }
};

/* Auto-initialize */
$nav.init();